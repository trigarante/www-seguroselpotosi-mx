module.exports = {
  target:'static',
  /*
  ** Headers of the page
  */
  head: {
    title: 'seguroselpotosi.mx',
    htmlAttrs:{lang: 'es-MX'},
    meta: [
      { charset: 'utf-8' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Potosí' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3bd600' },
  /*
  ** Build configuration
  */
  build: {
    
    /*
   ** Run ESLint on save
   */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/
        })
      }
    }
  },
  // include bootstrap css
  css: ['static/css/bootstrap.min.css','static/css/styles.css'],
  plugins: [{ src: '~/plugins/filters.js', ssr: false }],
  modules: 
      ['@nuxtjs/axios'], 
      // gtm:{ id: 'GTM-W4MS4RH' },
  env: {
    tokenData: '2/SfiAuJmICCW2WurfZsVbTvNsjLtrWdQ3gsHLqLSjE=',
    catalogo: "https://dev.ws-potosi.com", //CATALOGO
    motorCobro: "https://p.elpotosi-comprasegura.com/",
    sitio: "https://p.seguroselpotosi.mx",
    coreBranding: "https://dev.core-brandingservice.com", //CORE
    urlValidaciones: 'https://core-blacklist-service.com/rest', //VALIDACIONES
    urlMonitoreo: 'https://core-monitoreo-service.com/v1', //PRODUCCIÓN
    promoCore: "https://core-persistance-service.com/v1/"
  },
  render: {
    http2:{ push: true },
    resourceHints:false,
    compressor:{ threshold: 9 }
  }
}
