// import autosService from "./ws-autos";
import axios from 'axios'

const cotizacionService = {}

cotizacionService.search = function (data, accessToken) {
  return axios({
    method: "get",
    url: process.env.coreBranding + '/v1/extras/cotizar',
    headers: { Authorization: `Bearer ${accessToken}` },
    params: {request:data}
  })
}
export default cotizacionService


