import axios from 'axios'

let catalogo = process.env.catalogo + "/catalogos";
class CatalogosDirectos {
  marcas() {
    return axios({
      method: "get",
      url: catalogo + '/marcas_autos',
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: catalogo + `/modelos_autos?marca=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: catalogo + `/submarcas_autos?marca=${marca}&modelo=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    }
    );
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: catalogo + `/descripciones_autos?marca=${marca}&modelo=${modelo}&submarca=${submarca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    }
    );
  }
}

export default CatalogosDirectos;
