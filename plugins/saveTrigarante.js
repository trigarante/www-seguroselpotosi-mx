import axios from 'axios'

const url = process.env.coreBranding

class SaveService {
    saveProspecto(peticion, accessToken) {
        return axios({
            method: "post",
            headers: { 'Authorization': 'Bearer ' + accessToken },
            url: url + '/v3/cotizaciones/branding',
            data: JSON.parse(peticion)
        })
    }
    saveCotizacion(peticion, accessToken, cotizacionAli) {
        return axios({
            method: "put",
            headers: {'Authorization': 'Bearer '+accessToken},
            url: url + `/v1/cotizaciones_ali/${cotizacionAli}`,
            data: JSON.parse(peticion)
        })
    }
    saveLeadEmision(peticion, accessToken) {
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: url + '/v2/emisiones/solicitudes_online',
            data: JSON.parse(peticion)
          })
    }
    saveLeadInteraccion(peticion, accessToken){
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: url + '/v2/interacciones/solicitudes_online',
            data: JSON.parse(peticion)
          })
    }
    saveDataCliente(peticion, accessToken){
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: url + '/jsoncotizacionblanding/logdatos_cliente',
            data: JSON.parse(peticion)
        })
    }
}

export default SaveService