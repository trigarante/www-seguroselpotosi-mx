import axios from "axios"
 
//promoCore: "https://core-persistance-service.com/v1/"
 
 
async function telefonoService (idDiffusionMedium) {
    return axios({
        method: "get",
        url: process.env.promoCore + 'page/diffusion-medium/phone?idDiffusionMedium='+idDiffusionMedium,    
    })
}
 
export default telefonoService;
