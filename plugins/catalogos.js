import axios from 'axios'

let urlConsumo = process.env.coreBranding + '/v1'


class Catalogos {
  modelos(marca, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/el_potosi/modelos',
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{marca}
    })
  }
  submarcas(marca, modelo, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/el_potosi/submarcas',
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{marca,modelo}
    })
  }
  descripciones(marca, modelo, submarca, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/el_potosi/descripciones',
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{marca,modelo,submarca}
    })
  }
}

export default Catalogos;
