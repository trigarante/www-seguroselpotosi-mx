#! /bin/bash
grep -rl '"https://dev.core-brandingservice.com"' nuxt.config.js | xargs sed -i 's/\"https:\/\/dev.core-brandingservice.com\"/\"https:\/\/core-brandingservice.com\"/g'
grep -rl '"https://p.elpotosi-comprasegura.com/"' nuxt.config.js | xargs sed -i 's/\"https:\/\/p.elpotosi-comprasegura.com\/\"/\"https:\/\/elpotosi-comprasegura.com\/\"/g'
grep -rl '"https://p.seguroselpotosi.mx"' nuxt.config.js | xargs sed -i 's/\"https:\/\/p.seguroselpotosi.mx\"/\"https:\/\/seguroselpotosi.mx\"/g'
grep -rl '"https://dev.ws-potosi.com"' nuxt.config.js | xargs sed -i 's/\"https:\/\/dev.ws-potosi.com\"/\"https:\/\/ws-potosi.com\"/g'
